message-jdbc
===

> 对jdbc的封装

1. 配置
    ```xml
    <jdbc:jdbc dataSource="knowledgeMySQL" basePackage="knowledge" useFlyway="false" dbType="MySQL">
        <jdbc:convert key="solar.base.enums.DeleteFlag" value="solar.base.converts.DeleteFlagConvert"/>
    </jdbc:jdbc>
    ```
    - `dataSource`指的是数据源名称
    - `basePackage`基本包路径，用来做事务的切面
    - `useFlyway`是否使用flyway来对数据库做版本控制
    - `dbType`指定数据库类型，目前可选是`MySQL`、`Oracle`
    - `jdbc:convert`是来做pojo对象中得类型与数据库字段类型的转换

2. 关于`flyway`
    - 请参考[flyway的官网](http://flywaydb.org/)

3. 关于`jdbc:convert`
    - 我们有的时候在数据库中表示一种标识的时候通常会使用一个char类型的字段
    - 这种字段在Java中不建议去写死
    - 后来我们会使用静态常量（String）来表示
    - 这样也会有一些弊端，比如：去定义资源类型，太多了，实在不知道哪里是重复的
    - 所以就出现了枚举，将资源类型定义在一个枚举中，以后添加时一目了然
    - 这里的转换，一般情况会使用Java的枚举-->数据库的char类型/varchar类型
    - 枚举
    ```java
    // 必须实现message.base.convert.ConvertGetter这个接口
    public enum DeleteFlag implements ConvertGetter {
        DELETE_YES("1", "已删除"), DELETE_NO("0", "未删除");

        // 一般情况，构造器会定义成两个参数，一个是存在数据库中的值，一个是描述
        DeleteFlag(String value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        private String value;
        private String desc;

        public String getDesc() {
            return desc;
        }

        // 获取保存在数据库中的值
        @Override
        public String getValue() {
            return this.value;
        }
    }
    ```
    - 转换器
    1. 如果没有特殊情况,直接集成默认的转换器`message.jdbc.convert.DefaultConvert`就好了
    ```java
    public class SexConvert extends DefaultConvert<Sex> {
        public SexConvert(Class<Sex> type) {
            super(type);
        }
    }
    ```
    2. 特殊情况就实现指定接口:
    ```java
    // 实现接口，并且指定泛型类型为对应的枚举
    // message.jdbc.convert.Convert
    // 每个方法的含义请参考此接口
    public class DeleteFlagConvert implements Convert<DeleteFlag> {
        @Override
        public String getDbValue(DeleteFlag deleteFlag) {
            return deleteFlag.getValue();
        }

        @Override
        public String getDbNullValue(DeleteFlag deleteFlag) {
            return DeleteFlag.DELETE_NO.getValue();
        }

        @Override
        public DeleteFlag getPoJoValue(String o) {
            return getDeleteFlagValue(o);
        }

        @Override
        public DeleteFlag getPoJoNullValue(String o) {
            return DeleteFlag.DELETE_NO;
        }

        private DeleteFlag getDeleteFlagValue(String value){
            DeleteFlag[] deleteFlags = DeleteFlag.values();

            for(DeleteFlag deleteFlag : deleteFlags){
                if(value.equals(deleteFlag.getValue())) {
                    return deleteFlag;
                }
            }

            return null;
        }
    }
    ```
    - 然后在pojo对象中定义一个标识是否被删除的字段，当此字段为DeleteFlag.DELETE_YES时，存入数据库的值为字符串类型“1”

4. 关于CRUD
    1. 设置pojo对象
    ```java
    import javax.persistence.Column;
    import javax.persistence.GeneratedValue;
    import javax.persistence.Id;
    import javax.persistence.Table;

    // 数据库表名
    @Table(name = DB.TAG_TABLE_NAME)
    public class Tag {
        // 主键
        @Id
        // sequence名称
        @GeneratedValue(generator = DB.TAG_SEQ_NAME)
        private Long pkId;
        // 普通字段
        @Column
        private String name;

        // other fields
        // setter and getter methods
    }
    ```
    > 1. 数据库表名和sequence名称是必须要指定的
    > 2. 字段名称与数据库列名的对应关系：pkId --> pk_id
    > 3. 即遇到大写字母，将大写字母变小写，然后在前面加一个下划线(_)
    > 4. 如果注解里指定名称,则使用指定名称
    2. 关于CRUD的方法
        1. 自动装配注入`message.jdbc.GenericJdbcDAO`
        ```java
        @Autowired
        // 这里需要指定用哪个bean
        // dataSourceName + “GenericJdbcDAO”
        @Qualifier("knowledgeMySQLGenericJdbcDAO")
        private GenericJdbcDAO genericJdbcDao;
        ```
        2. `message.jdbc.GenericJdbcDAO`的主要方法
        ```java
        /**
         * 查询得到int型
         *
         * @param sql       query sql
         * @param params    need parameter
         * @return          result(int)
         * @throws org.springframework.dao.DataAccessException
         */
        public int queryForInt(String sql, Map params) throws DataAccessException;

        /**
         * 查询得到long型
         *
         * @param sql       query sql
         * @param params    need parameter
         * @return          result(long)
         * @throws org.springframework.dao.DataAccessException
         */
        public long queryForLong(String sql, Map params) throws DataAccessException;

        /**
         * update by sql
         *
         * @param sql
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public int update(String sql) throws DataAccessException

        /**
         * query by sql, given params type is <code>java.util.Map</code><br/>
         *
         * @param sql
         * @param params
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public int update(String sql, Map params) throws DataAccessException;

        /**
         * update sql when given parameter in an object
         *
         * @param sql
         * @param obj
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public int updateByBean(String sql, Object obj) throws DataAccessException;

        /**
         * query, return will be make as the class bean what you given
         *
         * @param sql
         * @param params
         * @param clazz
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public <T> T queryForBean(String sql, Map params, Class<T> clazz) throws DataAccessException;

        /**
         * query for a bean list as given class begin index is start, offset is num
         *
         * @param sql           query sql
         * @param start         begin index
         * @param num           offset
         * @param params
         * @param clazz         bean class
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public <T> List<T> queryForBeanList(String sql, int start, int num, Map params, Class<T> clazz) throws DataAccessException;

        /**
         * query for list
         *
         * @param sql       query sql
         * @param params    need parameter
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public <T> List<T> queryForList(String sql, Map params, Class<T> clazz) throws DataAccessException;

        /**
         * query for pagination
         *
         * @param sql           query sql, must be given
         * @param countSql      query count sql, maybe null
         * @param start         begin index
         * @param num           offset
         * @param params
         * @param clazz
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public <T> PaginationSupport<T> getBeanPaginationSupport(String sql, String countSql, int start, int num, Map params, Class<T> clazz) throws DataAccessException;

        /**
         * get next long pkId from database by given sequence
         *
         * @param name  sequence name
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public long generateLongId(String name) throws DataAccessException;

        /**
         * get next int pkId from database by given sequence
         *
         * @param name  sequence name
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public int generateIntId(String name) throws DataAccessException;

        /**
         * get next string pkId from database by given sequence
         *
         * @param name  sequence name
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public String generateStringId(String name) throws DataAccessException;

        /**
         * update
         * 不推荐使用，因为这个方法没有处理<code>convert</code>
         *
         * @param tableName        table name
         * @param columnParams      colum
         * @param whereParams       where
         * @return                  update column rows num
         * @throws org.springframework.dao.DataAccessException
         */
        public int commUpdate(String tableName, Map columnParams, Map whereParams) throws DataAccessException;

        /**
         * update
         *
         * @param tableName
         * @param columnParams
         * @return
         * @throws org.springframework.dao.DataAccessException
         */
        public int commUpdate(String tableName, Map columnParams) throws DataAccessException;

        /**
         * 彻底删除(默认主键为pk_id)
         *
         * @param tableName     表名
         * @param pkId          主键值
         * @throws Exception
         */
        public int commDelete(String tableName, Long pkId) throws Exception;

        /**
         * 彻底删除
         *
         * @param tableName     表名
         * @param keyColumn     主键名
         * @param pkId          主键值
         * @throws Exception
         */
        public int commDelete(String tableName, String keyColumn, Long pkId) throws Exception;

        /**
         * 安全软删除
         *
         * @param tableName         表名
         * @param keyColumn         主键名
         * @param pkId              主键值
         * @param deleteFlag        是否删除的标识位字段名
         * @return
         * @throws Exception
         */
        public int commSafeDelete(String tableName, String keyColumn, Long pkId, String deleteFlag) throws Exception;

        /**
         * 安全软删除(主键名默认为pk_id)
         *
         * @param tableName         表名
         * @param pkId              主键值
         * @param deleteFlag        是否删除的标识位字段名
         * @return
         * @throws Exception
         */
        public int commSafeDelete(String tableName, Long pkId, String deleteFlag) throws Exception;

        /**
         * 安全软删除(是否删除的标识位字段名默认为delete_flag)
         *
         * @param tableName         表名
         * @param keyColumn         主键名
         * @param pkId              主键值
         * @return
         * @throws Exception
         */
        public int commSafeDelete(String tableName, String keyColumn, Long pkId) throws Exception;

        /**
         * 安全软删除(主键名默认为pk_id,是否删除的标识位字段名默认为delete_flag)
         *
         * @param tableName         表名
         * @param pkId              主键值
         * @return
         * @throws Exception
         */
        public int commSafeDelete(String tableName, Long pkId) throws Exception;

        /**
         * 直接插入一个对象
         *
         * @param entity            要插入的对象
         * @return
         * @throws Exception
         */
        public <T> T genericInsert(T entity) throws Exception;

        /**
         * 获取一个对象根据主键
         *
         * @param <T>               对象类型
         * @param clazz             对象的class
         * @param key               主键
         * @return
         * @throws Exception
         */
        public <T> T genericLoad(Class<T> clazz, Serializable key) throws Exception;

        /**
         * 直接更新一个对象
         *
         * @param obj
         * @throws Exception
         */
        public void genericUpdate(Object obj) throws Exception;

        /**
         * 删除一个对象
         *
         * @param obj
         * @throws Exception
         */
        public void genericDelete(Object obj) throws Exception;

        /**
         * 根据主键删除一个对象
         *
         * @param clazz
         * @param key
         * @throws Exception
         */
        public void genericDelete(Class<?> clazz, Serializable key) throws Exception;
        ```
        3. 特殊用法：
        > 可以在pojo对象的类头上加上注解`@message.jdbc.annontations.Cache`即可支持在使用`generic`开头的几个方法时，将对象缓存起来，注意：必须要与message-cache结合使用
        > 1. 对注解`@message.jdbc.annontations.Cache`的说明：
        > - `long expire() default 31 * 24 * 3600` // 缓存中存在时间(默认31天)
        > - `String cacheRegion() default "ENTITY_REGION"` // cache存在的域(默认是ENTITY_REGION)

##依赖的cosmos项目
- message-base
- message-utils
- message-cache
- message-template

